from django.apps import AppConfig


class DjmockserverConfig(AppConfig):
    name = 'djmockserver'
