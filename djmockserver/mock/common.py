import os
from collections import OrderedDict
import json
import logging


logger = logging.getLogger(__name__)

def getfile(path: str, filelist: list):
    if os.path.isfile(path):
        # 筛选json文件
        if path.endswith('.json'):
            filelist.append(path)
    else:
        for file_or_dir in os.listdir(path):
            getfile(os.path.join(path, file_or_dir), filelist)

def read_mock(filelist: list):
    content = []
    try:
        for filename in filelist:
            with open(filename) as f:
                # 将json格式字符串输出原有顺序, 合并到content中
                content.extend(json.load(f, object_pairs_hook=OrderedDict))
    except Exception as e:
        logger.warning('read file failed : {}'.format(e))
    return content

def get_mock(path):
    logger.info('mock files path is {}'.format(path))
    filelist = []
    getfile(path, filelist)
    return read_mock(filelist)

if __name__ == '__main__':
    pass