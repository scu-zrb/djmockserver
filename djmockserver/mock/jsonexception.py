class BaseDiff(Exception):
    def __init__(self, diff_path:str, message:str=None):
        self.diff_path = diff_path
        self.message = message

    def __str__(self):
        return self.message

class TypeDiff(BaseDiff):
    """
    对比的元素类型不同
    """
    def __init__(self, diff_path:str, message:str=None):
        super().__init__(diff_path)
        self.message = message or 'element type is different, path is {}'.format(self.diff_path)

class LenDiff(BaseDiff):
    """
    对比列表长度不同
    """
    def __init__(self, diff_path:str, message:str=None):
        super().__init__(diff_path)
        self.message = message or 'length of list is different, path is {}'.format(self.diff_path)

class KeyDiff(BaseDiff):
    """
    字典key不同
    """
    def __init__(self, diff_path:str, message:str=None):
        super().__init__(diff_path)
        self.message = message or 'dict key is different, path is {}'.format(self.diff_path)

class ValueDiff(BaseDiff):
    """
    字典值,列表元素值不等
    """
    def __init__(self, diff_path:str, message:str=None):
        super().__init__(diff_path)
        self.message = message or 'element value is different, path is {}'.format(self.diff_path)



if __name__ == '__main__':
    try:
        raise TypeDiff('root')
    except Exception as e:
        print(e)

    try:
        raise KeyDiff('root.model.list.0')
    except Exception as e:
        print(e)

    try:
        raise ValueDiff('root.model.list.0')
    except Exception as e:
        print(e)

    try:
        raise LenDiff('root.model.list')
    except Exception as e:
        print(e)