# djmockserver

### 介绍

本程序是基于django开发的mock server, 用于构造动态的复杂的mock响应.

在我另外的项目<[BTest接口测试平台](https://gitee.com/scu-zrb/interface_test_platform)>中使用开源工具moco作为mock服务. 然而, moco对于实际应用中构造复杂动态的响应的需求, 似乎并没有很好的满足, 需要二次开发定制. 为了满足项目需要，就简单的开发了此程序. 

虽然简单, 但也实用. 对照moco的用法, 本程序也使用json文件来管理mock数据, 并可以使用多层目录来管理文件. 支持http/https.



### 特性说明
1. json格式保存mock数据(与moco类似),支持单文件或多层目录多文件管理数据文件
2. 根据请求内容匹配,同一uri可配置多种响应结果
3. mock数据修改实时更新无须重启服务(增删mock文件需重启)
4. 支持python字符串表达式(有白名单设置)执行,生成动态数据
5. 支持自定义函数用于构造响应数据
6. mock接口uri与真实uri保持一致
7. 支持非mock接口的请求转发


### 软件架构
![djmockserver 架构图](https://images.gitee.com/uploads/images/2020/0415/220619_cf15629e_5217681.png)


### 安装教程

下面提供两种方式：
- Dockerfile构建
- 开发环境安装


#### Dockerfile构建部署
1. 默认已有可用docker环境（自行解决）
2. 单独下载项目中Dockerfile文件到本地 或 git clone下载项目
3. 切到Dockerfile所在目录执行命令`docker build -t mockserver .` 和 `docker run -d --name=ms1 --restart=always -p 替换为要访问的端口:80 mockserver`

```bash
如正常，应该是如下输出：
[root@zrb mockserver]# docker build -t mockserver .
Sending build context to Docker daemon  3.584kB
Step 1/8 : FROM scuzrb/py3_dev:v1.0
 ---> 0917af555736
......
Step 8/8 : CMD ["uwsgi", "--ini", "uwsgi.ini"]
 ---> Running in 9d15e79bbc3c
Removing intermediate container 9d15e79bbc3c
 ---> 4920fa944331
Successfully built 4920fa944331
Successfully tagged mockserver:latest

[root@zrb mockserver]# docker images
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
mockserver            latest              4920fa944331        16 seconds ago      526MB
scuzrb/py3_dev        v1.0                0917af555736        10 days ago         489MB

[root@zrb mockserver]# docker run -d --name=ms1 --restart=always -p 9000:80 mockserver
70054e2903fdad702407c2e16590b414b4cf6760729fcb281b12668a6aac8907
[root@rabbit-api-001 mockserver]# 

[root@zrb mockserver]# docker ps -a
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS                  PORTS                                              NAMES
70054e2903fd        mockserver            "uwsgi --ini uwsgi.i…"   9 seconds ago       Up 8 seconds            0.0.0.0:9000->80/tcp                               ms1
```

4. 如上输出, 可在浏览器上输出`http://mock服务IP:访问端口/m6/` 访问mock, 正常会输出 `text --> content`. 到此服务已正常可用。
5. 如需要增加mock接口或拓展函数, 参考Dockerfile中说明。

#### 开发环境部署
1.  git clone下载项目
2.  切换到项目根目录 `pip install -r requirements.txt`
3.  配置mock数据文件,后缀为.json,可N个mock文件,支持多层目录管理
4.  修改配置 `djmockserverproject/settings.py` 文件(**文件路径必填**)
```
#必填 mock数据文件路径,可以是单个文件,或文件夹
MOCK_FILE_PATH = os.path.join(BASE_DIR, 'djmockserver', 'mockdata')
# MOCK_FILE_PATH = os.path.join(BASE_DIR, 'mock.json')

# mock数据文件刷新间隔, 单位:秒
REFRESH_INTERVAL = 2

# 选填 设置后，mock在匹配请求失败时，会把请求转发到此地址, 否则返回404
# REMOTE_HOST = 'http://127.0.0.1:8000'

# 选填 设置函数白名单列表
ADD_FUNC = ['len', 'str']
```
5.  运行服务 `python manage.py runserver`
6.  使用三方库拓展后可支持[https](https://blog.csdn.net/scu_07_bingo/article/details/105520828)

### 使用说明

----------

#### Mock文件字段


##### 1. request 支持的字段 (参考 RequestSerializer 类)
- uri: str
- method: str
- queries: dict
- form/json: dict
- body: str 非form和json格式的内容,如文本、html、xml等数据
- cookies: dict
- description: str 描述性文字

##### 2. response 支持的字段 (参考 ResponseMaker 类)
- status: int 响应码
- headers: dict 头域
- vars: dict 设置局部变量, 从上往下,优先级增加. 变量执行先于响应(text/json)
- duration: int/float/str 请求时延, 单位：秒
- text: str 
- json: dict 默认content-type为application/json
- text与json互斥,同在时json优先

----------

#### Mock文件格式


与moco格式一致, 以列表套字典的形式编写, 具体字段参考上面说明
```
[
    {
        "description": "",
        "request": {},
        "response": {}
    },
    {
        "description": "",
        "request": {},
        "response": {}
    }
]
```
可以多个文件保存多个URI响应列表(**此时应该配置MOCK_FILE_PATH为文件所在目录**)

----------

#### 构造动态响应



只要是符合***python***语法的***字串表达式***,且在白名单(变量函数)中,均可执行.

##### 1. 变量
- 只允许使用response中设置的局部变量和settings中设置的全局变量(如有).

##### 2. 响应
- 拓展函数(`djmockserver\mock\extend_func\extend_functions.py`): 自行拓展,定义函数,用于变量和响应中.

- 函数白名单(`ADD_FUNC`):为了安全考虑,限制python内置函数的使用.如要使用,需要添加函数到白名单中.

##### 3. mock配置样例
- 路径: `djmockserver\mockdata\`


----------

#### 提取请求



配合函数collection_get和变量req, 可以提取请求数据, 处理后作为响应内容返回. 

----------

#### 请求转发



主要是开发同学会用得上. 考虑到在实际开发中, host都是一处配置全局使用, 和uri分离. 而大多数的接口都有前置接口, 有上下文关系.

这时要使用mock,要么把全局host配置为mock地址而前置接口也mock掉,要么在被mock接口代码增加局部host(mock地址).

很明显两种方式都不好.这种情况下转发请求就比前面的更合适.

只需配置全局host为mock地址,并在本程序settings.py文件中配置转发地址即可.只要是未mock的请求都会转发.
![转发请求](https://images.gitee.com/uploads/images/2020/0415/220619_9c6a9dca_5217681.png)

#### TODO
1. 增加mock数据管理页
2. 更多协议的支持?


### 最后

代码设计地不够优雅, 还有很多地方有待斟酌, 基本满足工作.

