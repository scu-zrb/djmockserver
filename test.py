import requests

ip = '127.0.0.1'
port = 5555

# m1.json
url = 'http://{}:{}/m1/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == 'true'

url = 'http://{}:{}/m1/'.format(ip, port)
response = requests.post(url)
print('post', url)
print(response.status_code)
print(response.text)
assert response.text == 'false'


# m2.json
url = 'http://{}:{}/m2/'.format(ip, port)
response = requests.post(url, params={'a':'1', 'b':'2'})
print('post', url)
print(response.status_code)
print(response.text)
assert response.text == 'request use queries'

url = 'http://{}:{}/m2/'.format(ip, port)
response = requests.post(url, data={"a": "3", "b": "4"})
print('post', url)
print(response.status_code)
print(response.text)
assert response.text == 'request use form'

url = 'http://{}:{}/m2/'.format(ip, port)
response = requests.post(url, json={"a": "5", "b": "6"})
print('post', url)
print(response.status_code)
print(response.text)
assert response.text == 'request use json'


# m3.json
url = 'http://{}:{}/m3/test/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == 'uri use re'


# m4.json
url = 'http://{}:{}/m4/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == 'serve error'
assert response.status_code == 500


# m5.json
url = 'http://{}:{}/m5/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
print(response.headers['Content-Type'])
assert response.headers['Content-Type'] == 'text/html'


# m6.json
url = 'http://{}:{}/m6/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == 'text --> content'


# m7.json
url = 'http://{}:{}/m7/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.status_code == 200

jsondata = {
                "a": [
                    {"b":1}, {"b":2}
                ]
            }
url = 'http://{}:{}/m7/'.format(ip, port)
response = requests.post(url, json=jsondata)
print('post', url)
print(response.status_code)
print(response.text)
assert response.text == '{"value": "2"}'


# m8.json
url = 'http://{}:{}/lambda-is-forbidden/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == '(lambda : 1)()'

url = 'http://{}:{}/non-white-list-function/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == "__import__('os')"


# m9.json
url = 'http://{}:{}/white-list-function/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == "buildin function<len> and <str> are white list functions-->4"


# m10.json
url = 'http://{}:{}/use-attr/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == "1-->2"


# m11.json
url = 'http://{}:{}/m11/?c=123456'.format(ip, port)
data = {
    'a': [
        {
            'b': 'nothing here'
        },
        {
            'b': 'just matching uri when matched-item-list has only one item'
        }
    ]
}
response = requests.post(url, json=data)
print('post', url)
print(response.status_code)
print(response.text)
assert response.text == '{"from-json": "just matching uri when matched-item-list has only one item", "from-queries": "123456"}'

# m12.json
cookies = {"login": "true"}
url = 'http://{}:{}/m12/'.format(ip, port)
response = requests.get(url, cookies=cookies)
print('get', url)
print(response.status_code)
print(response.text)
assert response.text == '{"from-cookies": "true"}'


# m13.json  xml数据会保存到请求body属性中，但未支持解析和对比
xcontent = """
<?xml version="1.0" encoding="utf-8"?>
<note>
    <to id="1">zrb</to>
    <from id="2">hcj</from>
    <head>测试</head>
    <body>
        <p>点个关注吧</p>
    </body>
</note>
"""
url = 'http://{}:{}/m13/'.format(ip, port)
response = requests.post(url, data=xcontent.encode('utf8'), headers={'Content-Type': 'text/xml'})
print('post', url)
print(response.status_code)
print(response.text)


# m14.json
url = 'http://{}:{}/m14/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)


# m15.json
url = 'http://{}:{}/m15/'.format(ip, port)
response = requests.get(url)
print('get', url)
print(response.status_code)
print(response.text)
# assert response.text == 'this is a global var'
