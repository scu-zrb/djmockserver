# Dockerfile
# step1  docker build -t mockserver .
# step2  docker run -d --name=ms1 --restart=always -p 9000:80 mockserver

FROM scuzrb/py3_dev:v1.0
MAINTAINER zrb

WORKDIR /home/app

# 1. git update code
RUN ["git", "clone", "https://gitee.com/scu-zrb/djmockserver.git"]

# 2. copy all json file to container mockdata dir if you have mockdata
# COPY ["mockdata/*.json", "/home/app/djmockserver/djmockserver/mockdata/"]

# 3. copy extend function file to container 
# COPY ["extend_functions.py", "/home/app/djmockserver/djmockserver/mock/extend_func/"]

# 4. copy settings file
# COPY ["settings.py", "/home/app/djmockserver/djmockserverproject/settings.py"]

# 5. WORKDIR
WORKDIR /home/app/djmockserver

# 6. extend requirements.txt
# COPY ["extend_req.txt", "extend_req.txt"]

# 7. pip3
RUN pip3 install -r requirements.txt 
# RUN pip3 install -r extend_req.txt

# 8. copy uwsgi.ini if override
# COPY ["uwsgi.ini", "uwsgi.ini"]

# 9. EXPOSE
EXPOSE 80

# run
CMD ["uwsgi", "--ini", "uwsgi.ini"]
